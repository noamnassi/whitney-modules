import io
import time
import tarfile
import logging
from threading import Thread
from abc import ABCMeta, abstractproperty, abstractmethod

class WhitneyModule(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self._module_name = self.__class__.__name__
        self._conf_filePath = "/etc/modulesconf/%s.conf" % self._module_name
        
        log_path = "/tmp/%s.log" % self._module_name
        logging.basicConfig(filename=log_path)
        self._logger = logging.Logger(self._module_name)
        self._should_exit = False

    @abstractproperty
    def VERSION(self):
        pass

    @abstractproperty
    def CONFIG_INTERVAL(self):
        pass

    @abstractmethod
    def check_configure(self):
        pass

    @abstractproperty
    def REPORT_INTERVAL(self):
        pass

    @abstractmethod
    def check_report(self):
        pass

    @abstractproperty
    def UPDATE_INTERVAL(self):
        pass

    @abstractmethod
    def check_update(self):
        pass

    @staticmethod
    def configure_loop(module_object):
        while module_object._should_exit is False:
            try:
                module_object.check_configure()
            except Exception as e:
                module_object._logger(e)
            time.sleep(module_object.CONFIG_INTERVAL)

    @staticmethod
    def report_loop(module_object):
        while module_object._should_exit is False:
            try:
                module_object.check_report()
            except Exception as e:
                module_object._logger(e)
            time.sleep(module_object.REPORT_INTERVAL)

    @staticmethod
    def _create_loop_thread(func, params):
        thread = Thread(target=func, args=params)
        thread.start()
        return thread

    def start(self):
        config_thread = self._create_loop_thread(WhitneyModule.configure_loop, (self,))
        report_thread = self._create_loop_thread(WhitneyModule.report_loop, (self,))

        # Making sure that the threads run
        while self._should_exit is False:
            if config_thread.is_alive is False:
                config_thread = self._create_loop_thread(WhitneyModule.configure_loop, (self,))
            if report_thread.is_alive is False:
                report_thread = self._create_loop_thread(WhitneyModule.report_loop, (self,))

            try:
                self.check_update()
            except Exception as e:
                self._logger.error(e)
            time.sleep(self.UPDATE_INTERVAL)

        config_thread.join()
        report_thread.join()

    def get_timestamp(self):
        with open(self._conf_filePath, 'r') as conf_file:
            return float(conf_file.readline())

    def set_timestamp(self, timestamp):
        with open(self._conf_filePath, 'w') as conf_file:
            conf_file.write(timestamp)

    def install_update(self, update_archive):
        archive_as_file = io.BytesIO(update_archive)
        with tarfile.open(fileobj=archive_as_file) as update_tar:
            update_tar.extractall("/lib/whitneymodules/")
        self._should_exit = True
